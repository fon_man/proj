import {createStore} from 'vuex';

export default createStore({
  state: {
    urls: {
      back__root: 'http://localhost:8005',
      forum_read_track_filter: '/test/forum_read_track/filter/',
      forum_read_track: '/test/forum_read_track/',
      forum: '/test/forum/?format=json',
      user: '/test/user/?format=json',
    },
    pagination_count: 10,
  },
  mutations: {},
  actions: {},
  modules: {},
  getters: {},
});
