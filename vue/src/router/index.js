import {createRouter, createWebHistory} from 'vue-router';
import ForumReadTrackList from '../views/ForumReadTrackList';
import ForumReadTrackElement from '../views/ForumReadTrackElement';

const routes = [
  {
    path: '/',
    name: 'ForumReadTrackList',
    component: ForumReadTrackList,
  },
  {
    path: '/:id',
    name: 'ForumReadTrackElement',
    component: ForumReadTrackElement,
    props: true,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
