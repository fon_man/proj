from __future__ import absolute_import, unicode_literals
from celery import shared_task


@shared_task(ignore_result=True)
def send_mail_task(title, message, email_to, email_from='from@example.com'):
    """Задача для отправки письма"""
    from django.core.mail import send_mail
    send_mail(title, message, email_from, [email_to])
