from rest_framework import serializers
from machina.apps.forum_tracking.models import ForumReadTrack
from machina.apps.forum.models import Forum
from django.contrib.auth import models as auth_models


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = auth_models.User
        fields = ('id', 'username')


class ForumSerializer(serializers.ModelSerializer):
    class Meta:
        model = Forum
        fields = ('id', 'name')


class ForumReadTrackSerializer(serializers.ModelSerializer):
    # user = UserSerializer()
    # forum = ForumSerializer()

    class Meta:
        # depth = 1
        model = ForumReadTrack
        fields = '__all__'


class ForumReadTracFilterkSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    forum = ForumSerializer()

    class Meta:
        # depth = 1
        model = ForumReadTrack
        fields = ('id', 'user', 'forum')


class ForumReadUsersSerializer(serializers.ModelSerializer):
    tracks = ForumReadTrackSerializer(many=True)

    class Meta:
        model = Forum
        fields = ('id', 'name', 'tracks')
