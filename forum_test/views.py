from machina.apps.forum_tracking.models import ForumReadTrack
from machina.apps.forum.models import Forum
from rest_framework import viewsets, filters, generics, permissions
from rest_framework.decorators import action
from . import serializers, tasks
from django.contrib.auth import models as auth_models


class ForumReadUsersViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows ForumReadTrack to be viewed or edited.
    """
    queryset = Forum.objects.all()
    serializer_class = serializers.ForumReadUsersSerializer


class ForumReadTrackViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows ForumReadTrack to be viewed or edited.
    """
    permission_classes = [permissions.AllowAny]
    queryset = ForumReadTrack.objects.all()
    serializer_class = serializers.ForumReadTrackSerializer

    def create(self, request, *args, **kwargs):
        tasks.send_mail_task.apply_async(
            ['user start read new forum', 'Here is the message.', 'to@example.com'],
            countdown=1,
            expires=60
        )
        return super(ForumReadTrackViewSet, self).create(request)

    @action(detail=False)
    def filter(self, request, *args, **kwargs):
        queryset = ForumReadTrack.objects.all()

        user_id = self.request.query_params.get('user_id')
        forum_id = self.request.query_params.get('forum_id')
        forum_read_track_id = self.request.query_params.get('forum_read_track_id')

        queryset = queryset.filter(user_id=user_id) if user_id else queryset
        queryset = queryset.filter(forum_id=forum_id) if forum_id else queryset
        queryset = queryset.filter(id=forum_read_track_id) if forum_read_track_id else queryset

        page = self.paginate_queryset(queryset)
        serializer = serializers.ForumReadTracFilterkSerializer(page, many=True)
        return self.get_paginated_response(serializer.data)



class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows User to be viewed or edited.
    """
    queryset = auth_models.User.objects.all()
    serializer_class = serializers.UserSerializer
    paginator = None


class ForumViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Forum to be viewed or edited.
    """
    queryset = Forum.objects.all()
    serializer_class = serializers.ForumSerializer
    paginator = None
